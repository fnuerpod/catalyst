#!/bin/bash

# PASS ENVIRONMENT VARIABLE "OPEN_RC" TO IGNORE SYSTEMD-ONLY COMMANDS!!!
# If adjustable value OPENRC_MODE is set, this is ignored.

# Adjustable values:
DEFAULT_ROOT_PASSWORD="87658765XeniaLinux"

RECOVERY_PASSWORD_HASH="$6$ovJXS/P4rKaURNaD$IUmaP2JW5uiJgrFVr31bEMb6kEF.ARL.x23m.qvyJ3.oRRbJ1qQ/pU5R2VocEzunYqSGF/YvLFGqF5gn0BQY90"

SERVICES_ENABLE=( "bluetooth" "NetworkManager" "cups" "systemd-timed" "gdm" "lvm2-monitor" "qemu-guest-agent" "spice-vdagentd" "zfs.target" "zfs-import-cache" "zfs-mount" "zfs-import.target" )

SERVICES_GLOBAL_ENABLE=( "pipewire.socket" "pipewire-pulse.socket" "wireplumber.service" )

SET_ROOT_OWNERSHIP=( "/" )

SET_ROOT_OWNERSHIP_RECURSIVELY=( "/etc/" "/boot" "/overlay" "/roots" "/usr" "/var" )

OPENRC_MODE=false

############ USERS SHOULDN'T NEED TO TOUCH ANYTHING BEYOND THIS POINT ############

# Run dracut.
dracut --force --no-hostonly --kver $(ls /lib/modules)

# Set default root password and set sudoers ownership.
echo "root:${DEFAULT_ROOT_PASSWORD}" | chpasswd
chown root:root /etc/sudoers

# Setup recovery.
cp /etc/passwd /.recovery/etc/passwd
cp /etc/shadow /.recovery/etc/shadow

echo "recovery:x:1000:1000::/home/recovery:/bin/bash" >> /.recovery/etc/passwd
echo "recovery:${RECOVERY_PASSWORD_HASH}:19574::::::" >> /.recovery/etc/shadow

sed s/wheel:x:10:root/wheel:x:10:root,recovery/ /etc/group > /.recovery/etc/group
echo "recovery:x:1000:" >> /.recovery/etc/group

chown 1000:1000 -R /.recovery/home/recovery
# End setup recovery.

# Set ownership of distribution files in cache to portage's user.
chown portage:portage /var/cache/distfiles

# Setup locale.
cp /usr/share/i18n/SUPPORTED /etc/locale.gen
locale-gen

# Only run systemd commands when building a systemd image, as specified above.
if [[ -d /lib/systemd/system ]]; then
    # Enable all services to be enabled.
    for services in ${SERVICES_ENABLE[@]}; do 
        systemctl enable $services
    done

    # Enable all services to be enabled GLOBALLY
    for services in ${SERVICES_GLOBAL_ENABLE[@]}; do
        systemctl --global enable $services
    done
fi

# Delete old boot files.
rm /boot/*.old

# Copy latest boot files.
cp /boot/vmlinuz* /boot/vmlinuz
cp /boot/initramfs* /boot/initramfs.img
cp /boot/System* /boot/System.map
cp /boot/config* /boot/config

# Add flatpak repository.
flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

# Perform ownership change to root (from uid/gid 1001).
for file in ${SET_ROOT_OWNERSHIP[@]}; do
    chown --from=1001:1001 root:root $file 
done

# Perform recursive ownership change to root (from uid/gid 1001).
for file in ${SET_ROOT_OWNERSHIP_RECURSIVELY[@]}; do
    chown --from=1001:1001 root:root $file -R
done

# Perform ownership change to root (from uid/gid 1000).
for file in ${SET_ROOT_OWNERSHIP[@]}; do
    chown --from=1000:1000 root:root $file 
done

# Perform recursive ownership change to root (from uid/gid 1000).
for file in ${SET_ROOT_OWNERSHIP_RECURSIVELY[@]}; do
    chown --from=1000:1000 root:root $file -R
done

